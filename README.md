# Chat react app with firebase


## generando la aplicacion por defecto

empezamos creando un directorio en proyectos y nos movemos dentro
-
```javascript
  mkdir reactchatapp
  cd reactchatapp
```

para agilizar utilizaremos una semilla de proyecto que nos facilitara tener una app por defecto puedes ver la info en 
-
```
       https://github.com/facebookincubator/create-react-app
```

para instalarla utiliza
-
```
       npm install -g create-react-app
```

para generar el scafolding de la aplicacion (el . es para que use el mismo directorio donde estas)
-
```
       create-react-app .
```

comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador veras una pagina con el logo de react
-
```
       http://localhost:3000
```

## incluir en dependencias bootstrap y jquery, vamos a instalarlas con npm
-
~~~ 
     npm install bootstrap jquery -save
~~~

tiene que aparecer en el package .json 
-
~~~ 
   "dependencies": {
     "bootstrap": "^3.3.7",
     "jquery": "^3.1.1",  
~~~

eliminar el css por defecto del app.js
-
~~~ 
  .App {
    text-align: center;
  }

  .App-logo {
    animation: App-logo-spin infinite 20s linear;
    height: 80px;
  }

  .App-header {
    background-color: #222;
    height: 150px;
    padding: 20px;
    color: white;
  }

  .App-intro {
    font-size: large;
  }

  @keyframes App-logo-spin {
    from { transform: rotate(0deg); }
    to { transform: rotate(360deg); }
  }    
~~~

poner una capa como contenido del render en vez del contenido pro defecto (File src/App.js)
-
~~~ 
    import React, { Component } from 'react';
    import './App.css';

    class App extends Component {
      render() {
        return (
          <div>
           hello word
          </div>
        );
      }
    }

    export default App;  
~~~

borrar el fichero /src/logo.svg que ya no usaremos

comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador:
-
```
       http://localhost:3000
```
deberias de ver el saludo en vez del contenido por defecto

## importar librerias externas en la aplicacion para poder usarlas

en el fichero ( src/index.js ) incluimos las dependencias de jquery y bootstrap
-
~~~ javascript
  import React from 'react';
  import ReactDOM from 'react-dom';
  import App from './App';

  // importar el modulo
  import 'jquery';
  // definirlo como variable global disponible en toda la app 
  global.jQuery = require('jquery');
  // importar bootstrap (el modulo)
  require('bootstrap');
  // traer el css de bootstrap en la app 
  import 'bootstrap/dist/css/bootstrap.css';

  import './index.css';

  ReactDOM.render(
    <App />,
    document.getElementById('root')
  );  
~~~

## incluir un panel en la aplicacion 
-
copiamos el ejemplo de panel de bootstrap en el render de App.js, pero debemos remplazar class por className ya que class es palabra reservada de Javascript
-
~~~ 
    import React, { Component } from 'react';
    import './App.css';

    class App extends Component {
      render() {
        return (
          <div>
            <div className="panel panel-primary"> 
              <div className="panel-heading"> 
                <h3 className="panel-title">Escribe tu mensaje</h3> 
              </div> 
              <div className="panel-body"> Panel content </div> 
            </div>
          </div>
        );
      }
    }

    export default App;
~~~

comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador:
-
```
       http://localhost:3000
```
deberias ver el panel puesto con su cabecera en azul 


## incluir estilos para hacerlo responsive 

creamos una clase post-editor (src/App.css)
-
~~~ 
  .post-editor{
   margin:0 20%;
  }
  @media(max-width: 900px){
    .post-editor{
      margin: 1em;
    }
  }
~~~

incluimos la clase en el panel 
-
~~~ 
    import React, { Component } from 'react';
    import './App.css';

    class App extends Component {
      render() {
        return (
          <div>
            <div className="panel panel-primary post-editor"> 
              <div className="panel-heading"> 
                <h3 className="panel-title">Escribe tu mensaje</h3> 
              </div> 
              <div className="panel-body"> Panel content </div> 
            </div>
          </div>
        );
      }
    }

    export default App; 
~~~

comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador:
-
```
       http://localhost:3000
```
deberias de ver el panel centrado y si cambias el tamano de la ventana este se recoloca centrado


## incluir la caja para escribir el mensaje y el campo para el usuario que envia el mensaje

cambiamos el contenido del panel por un input, un text area y un boton con las clases de bootstrap "input-group", "form-control" y "btn-success" ademas agregamos una clase personalizada a cada elemento del formulario para facilmente cambiar sus estilos
-
~~~ 
    import React, { Component } from 'react';
    import './App.css';

    class App extends Component {
      render() {
        return (
          <div>
            <div className="panel panel-primary post-editor"> 
              <div className="panel-heading"> 
                <h3 className="panel-title">Escribe tu mensaje</h3> 
              </div> 
              <div className="panel-body"> 
                   <div className="input-group user-name">
                      <span className="input-group-addon" id="basic-addon1">Tu nombre</span>
                      <input className="form-control " placeholder="Usuario"/>
                  </div>

                 <textarea className="form-control post-editor-input" ></textarea>

                 <button type="button" className="text-right btn btn-success post-editor-button">Enviar</button>
                 
              </div> 
            </div>
          </div>
        );
      }
    }

    export default App; 
~~~

ponemos un css sobreescrito para el boton ( src/App.css) y para el input de nombre de usuario
-
~~~ 
    .post-editor-button{
      float:right;
      margin-top: 10px;
    } 
    .user-name{
      margin-bottom:20px;
    }  
~~~


comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador:
-
```
       http://localhost:3000
```
deberias de ver el campo para que el usuario agregue su nombre y el text area para escribir con el boton debajo 


## incluir un panel para los mensajes de los usuarios


incluimos encima del anterior un panel que tenga dentro un componente de alert de bootstrap en el (src/App.js)
-
~~~ 
        <div className="panel panel-primary post-editor"> 
          <div className="panel-heading"> 
            <h3 className="panel-title">Mensajes en el chat</h3> 
          </div> 
          <div className="panel-body"> 
            <div className="alert alert-info" role="alert">
	       <strong>Usuario : </strong> Hola mundo!
	   </div>
          </div> 
        </div>  
~~~


comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador:
-
```
       http://localhost:3000
```
deberias de ver el panel de comentarios con el que pusimos por defecto


## refactor de app a un componente Post separado

en la carpeta src creamos una carpeta para los "Post"
-
~~~ 
      mkdir Post
~~~

nos movemos dentro y creamos una component 
-
~~~ 
    cd Post
    mkdir component 
~~~

nos movemos dentro y creamos un archivo jsx llamado Post
-
~~~ 
     cd component
~~~

importamos React y creamos el componente
-
~~~ 
import React from 'react';

const Post = (props) => (


)
~~~

a continuacion cortamos el html del mensaje y lo pegamos dentro del componente 
-
~~~ 
  import React from 'react';

  const Post = (props) => (
    <div className="panel-body"> 
       <div className="alert alert-info" role="alert">
	 <strong>Usuario : </strong> Hola mundo!
       </div>
    </div> 
  );  
~~~

y lo exportamos para poder usarlo 
-
~~~ 
    export default Post; 
~~~

vamos al (app.js) e importamos el componente nuevo
-
~~~ 
     import Post from './Post/component/Post';
~~~

donde teniamos el html ponemos la tag del componente
-
~~~ 
  <div className="panel panel-primary post-editor"> 
    <div className="panel-heading"> 
      <h3 className="panel-title">Mensajes en el chat</h3> 
    </div> 
   <Post />
  </div>  
~~~

comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador:
-
```
       http://localhost:3000
```
deberias de ver el panel con el mensaje igual que antes pero ahora utilizando un componente 


## tener un estado (propiedad) con un listado de mensajes


agregamos un constructor al (App.js) con un primer mensaje puesto ahi a mano en su propiedad posts
-
~~~ 
  constructor(props) {
    super(props);
    this.state = {
      posts : [{name: "Juanito", message: "Estoy en Activ"}]
    }
  }
~~~

y sustituimos la llamada al componente de <Post/> una vez por cada mensaje que tengamos en el array (idx es para que exista un indice diferente para cada elemento)
-
~~~ 
          {
                this.state.posts.map((item, idx) => {
                  return (<Post key={idx}  userName={item.name}  postBody={item.message} />)
                })
              }
~~~

y hacemos que en Post.jsx se use la variable pasada
-
~~~ 
  import React from 'react';

  const Post = (props) => (
    <div className="alert alert-info" role="alert">
        <strong>{ props.userName } : </strong> { props.postBody }
    </div>
  );
  export default Post;   
~~~


comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador:
-
```
       http://localhost:3000
```
deberias de ver el panel con el mensaje de Juanito


## poner la logica para detectar cuando se crea un mensaje y pasarlo como Post

borramos el valor por defecto de los mensajes  y creamos una propiedad nueva para el valor que el user pone en el text area y el el campo user name
-
~~~ 
     this.state = {
        posts : [],
        newPostBody: '',
        newPostUser: ''
      }
~~~

creamos en App.js una funcion nuevas, para controlar el click del boton, cuando se cambie el valor del textarea y del campo de usuario
-
~~~ 
  addPost () {

  }
handlePostEditorInputChange(ev){

  }

  handleUserNameInputChange(ev){

  }
~~~ 

para conseguir que el ambito dentro de las funciones sea el componente y no window en el constructor utilizamos el hack para bindear un nuevo bind
-
~~~ 
  constructor(props) {
    super(props);
    
    // hack para asegurar que las funciones tiene el contexto del componente
    this.addPost = this.addPost.bind(this);
    this.handlePostEditorInputChange = this.handlePostEditorInputChange.bind(this);
    this.handleUserNameInputChange = this.handleUserNameInputChange.bind(this);

    this.state = {
      posts: [],
      newPostBody: '',
      newPostUser: ''
      
    }
  }  
~~~

cambiamos el html del text area, del input de usuario y del input de usuario para que llamen a sus funciones cuando el usuario los cambie y para que refleje el valor si cambiamos el estado 
-
~~~ 
      <div className="input-group user-name">
         <span className="input-group-addon" id="basic-addon1">Tu nombre</span>
           <input value={this.state.newPostUser} 
                  onChange={this.handleUserNameInputChange}  
                  type="text" className="form-control " placeholder="Usuario"/>
        </div>

        <textarea value={this.state.newPostBody} 
             onChange={this.handlePostEditorInputChange} 
             className="form-control post-editor-input" > 
        </textarea>
~~~

cambiamos el boton para que llame a la funcion que gestiona el agregar un nuevo Post
-
~~~ 
       <button 
         onClick={this.addPost} 
         type="button" 
         className="btn btn-success post-editor-button">
         Post
      </button>        
~~~

actualizamos el estado cuando el usuario esta escribiendo en el text area y cuando escribe en el campo de usuario
-
~~~ 
  handlePostEditorInputChange(ev){
    this.setState({
      newPostBody: ev.target.value
    });
  }
  handleUserNameInputChange(ev){
    this.setState({
      newPostUser: ev.target.value
    });
  }
~~~

actualizamos el array de post cuando el usuario pulsa el boton, notese que creamos una copia del estado para cambiarla antes de asignarla de nuevo
-
~~~ 
  addPost () {
    // copiar el estado
    const newState = Object.assign({}, this.state);
    // incluir el nuevo mensaje
    newState.posts.push({name: this.state.newPostUser, message: this.state.newPostName});
    // vaciar el input, solo el del mensaje, pues el usuario queremos que siga siendo el mismo
    newState.newPostBody = '';
    // actualizar el estado
    this.setState(newState);
  }
~~~
comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador:
-
```
       http://localhost:3000
```
deberias de poder escribir en el text area  y al pulsar el boton se aparece el mensaje, si haces varias veces cada vez se ve el nuevo estado


## refactorizar para tener un componente de envio de post 

creamos un directorio para el nuevo componente 
-
~~~ 
  cd src
  mkdir PostEditor
  cd PostEditor
  mkdir component
  cd component
  touch   PostEditor.jsx
~~~

creamos una clase dentro 
-
~~~ 
import React, { Component } from 'react';


class PostEditor extends Component {
  
}

export default PostEditor;     
~~~

queremos que el elemento tenga su estado asi que copiamos el constructor de App.js 
-
~~~ 
    constructor(props) {
    super(props);
    
       // hack para asegurar que las funciones tiene el contexto del componente
    this.addPost = this.addPost.bind(this);
    this.handlePostEditorInputChange = this.handlePostEditorInputChange.bind(this);
    this.handleUserNameInputChange = this.handleUserNameInputChange.bind(this);


        this.state = {
          posts : [],
          newPostBody: '',
          newPostUser: ''
         } 
  }   
~~~


copiamos el html del componente que es el editor de mensaje en el metodo render

-
~~~ 
  render () {
    return (
        <div className="panel-body"> 
                  <div className="input-group user-name">
                    <span className="input-group-addon" id="basic-addon1">Tu nombre</span>
                    <input value={this.state.newPostUser} 
                    onChange={this.handleUserNameInputChange}  
                    type="text" className="form-control " placeholder="Usuario"/>
                  </div>

                  <textarea value={this.state.newPostBody} 
                    onChange={this.handlePostEditorInputChange} 
                    className="form-control post-editor-input" > 
                  </textarea>

                 <button
                 onClick={this.addPost}  
                 type="button" 
                 className="btn btn-success post-editor-button">
                 Enviar
                 </button>
               </div> 
    )
  }     
~~~

en el html de la app ponemos la referencia a nuestro componente en lugar del html anterior y le pasamos la funcion que debe de invocar 
-
~~~ 
    <div className="panel panel-primary post-editor"> 
       <div className="panel-heading"> 
           <h3 className="panel-title">Escribe tu mensaje</h3> 
        </div>

        <PostEditor addPost={this.addPost} />
     </div>
~~~

para poder usarlo hay que importarlo 
-
~~~ 
     import PostEditor from './PostEditor/component/PostEditor';
~~~


movemos el metodo de controlar el post y el del controlar el usuario de la App.js al componente  PostEditor.jsx 
-
~~~ 
   handlePostEditorInputChange(ev){
    this.setState({
      newPostBody: ev.target.value
    });
  }
  handleUserNameInputChange(ev){
    this.setState({
      newPostUser: ev.target.value
    });
  }
~~~

creamos una funcion local para el post llamado createPost
-
~~~ 
createPost (){
    this.props.addPost(this.state.newPostBody, this.state.newPostUser);
    this.setState({newPostBody: ''});
  }
~~~


y cambiamos metodo onclick del boton para que utilice la funcion 
-
~~~ 
      onClick={this.createPost} 
~~~

no olvidar el binding en el constructor y quitar el del addPost
-
~~~ 
  this.createPost = this.createPost.bind(this);
~~~

eliminamos en el App.js las referencias a las funciones movidas al componente y lo de limpiar el input 
-
~~~ 
  constructor(props) {
    super(props);
    
    // hack para asegurar que las funciones tiene el contexto del componente
    this.addPost = this.addPost.bind(this);

    this.state = {
      posts: []
    }
  }
~~~

y actualizamos el metodo addPost para que tenga como parametros lo que le pasa el componente PostEditor
-
~~~ 
     
   addPost(newPostBody, newPostUser) {
    // copiar el estado
    const newState = Object.assign({}, this.state);
    // incluir el nuevo mensaje
    newState.posts.push({name: newPostUser, message: newPostBody});
    // actualizar el estado
    this.setState(newState);
  }

~~~
comprobamos que todo funciona ok
-
```
       npm start
```
si entras en el navegador:
-
```
       http://localhost:3000
```
deberias de poder escribir en el text area, poner tu usuario y que se vea en la parte de mensajes del chat

## crear un proyecto en firebase

Creamos una cuenta gratuita en firebase, pulsamos en ir a la consola, pulsamos en el boton de crear proyecto y rellenamos el formulario
-
~~~ 
    Crear proyecto
    Nombre del proyecto

    reactchatapp
    País/Región help_outline
    México
~~~

Con el proyecto creado pulsamos añadir base de datos al proyecto y nos ofrece la configuracion 
-
~~~ 
     <script src="https://www.gstatic.com/firebasejs/3.7.3/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyDKyT4b8hPU17ApCxCyHw7Q29Ho2cv_O_8",
    authDomain: "reactchatappdemo.firebaseapp.com",
    databaseURL: "https://reactchatappdemo.firebaseio.com",
    projectId: "reactchatappdemo",
    storageBucket: "reactchatappdemo.appspot.com",
    messagingSenderId: "22833372156"
  };
  firebase.initializeApp(config);
</script>
~~~

ahora es necesario poner firebase en nuestra app
-
~~~ 
     npm install --save firebase
~~~


### poner firebase en nuestra app 

vamos al App.js y importamos firebase
-
~~~ 
    import firebase from 'firebase/app';
    import 'firebase/database';
~~~

En el constructor del App.js incluimos la llamada de inicializar firebase antes de nuestras funciones
-
~~~ 
constructor(props) {
    super(props);
....
// config copiado de firebase ya que hemos creado ahi el proyecto
    const config = {
    apiKey: "AIzaSyDKyT4b8hPU17ApCxCyHw7Q29Ho2cv_O_8",
    authDomain: "reactchatappdemo.firebaseapp.com",
    databaseURL: "https://reactchatappdemo.firebaseio.com",
    projectId: "reactchatappdemo",
    storageBucket: "reactchatappdemo.appspot.com",
    messagingSenderId: "22833372156"
  };
// referencia al objeto de firebase
    this.app = firebase.initializeApp(config);
//referencia a la base de datos
    this.database = this.app.database();
// referencia a la "tabla" post
    this.databaseRef =this.database.ref().child('post');
  }
~~~


cuando el componente se carga  actualizamos el estado agregando una funcion 
-
~~~ 
  componentWillMount() {
    const {updateLoacState} = this;
    this.databaseRef.on('child_added', snapshot => {
       const response = snapshot.val();
       updateLoacState(response);
    });
  }

~~~

creamos una funcion para actualizar el estado de los posts desde firebase y no desde nuestro local
-
~~~ 
 updateLoacState(response) {
   // copia del estado actual
    const posts = this.state.posts;
    // separar por saltos de linea (no se ven en html)
     //const brokenDownPost = response.message.split(/[\r\n]/g);
    // actualizar la copia del estado
  posts.push(response);
    // actualizar el estado
    this.setState(posts);
  } 
~~~

cambiamos addPost para que guarde en firebase
-
~~~ 
  addPost (newPostBody, newPostUser) {
    // copiar el estado
    const postToSave = {name: newPostUser, message: newPostBody};
   // guardar en firebase nuestros posts
   this.databaseRef.push().set(postToSave);
  }
~~~

vamos a firebase, en el menu lateral escogemos database y en la url ponemos 
-
~~~ 
   https://reactchatappdemo.firebaseio.com/post
~~~
se deberia de ver null
-
~~~ 
   null
~~~

lo ejecutamos 
-
~~~ 
   npm start 
~~~

y tiene que fallar por permisos
-
~~~ 
   Uncaught (in promise) Error: PERMISSION_DENIED: Permission denied
~~~

vamos a firebase, a la pestaña reglas y las cambiamos ambas a true
-
~~~ 
{
  "rules": {
    ".read": "true",
    ".write": "true"
  }
}
~~~

por ultimo para poder ver online nuestra app, vamos a usar la opcion de hosting del mismo firebase e instalamos firebase tools
-
~~~

npm install -g firebase-tools

~~~

Hacemos login con nuestras credenciales de firebase
-
~~~

firebase login

~~~

Inicializamos firebase en el proyecto 
-
~~~

firebase init

~~~

Y Por ultimo antes de subir a firebase ejecutamos el comando que crea la version de produccion de nuestra app
-
~~~
npm run build

~~~

Una vez hecho esto ya tenemos un folder build que es el que usaremos como folder de inicio de nuestra app, ya podemos terminar ejecutando:
-

~~~
firebase deploy

~~~

##Con esto habremos subido nuestra app al hosting gratuito de firebase y podemos verlo en la URL que nos proporciono
-
~~~
https://reactchatappdemo.firebaseio.com/
~~~